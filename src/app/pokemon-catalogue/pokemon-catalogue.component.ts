import {Component, OnInit} from '@angular/core';
import {PokemonsService} from "../services/pokemons.service";
import {Pokemon} from "../models/pokemon.model";
import {SessionService} from "../services/session.service";
import {TrainersService} from "../services/trainers.service";
import {PageEvent} from "@angular/material/paginator";
import {Router} from "@angular/router";

@Component({
    selector: 'app-pokemon-catalogue',
    templateUrl: './pokemon-catalogue.component.html',
    styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit {

    pokemonArray: Pokemon[] | undefined;
    pageSlice: Pokemon[] | undefined;

    constructor(
        private readonly pokemonsService: PokemonsService,
        private readonly sessionService: SessionService,
        private readonly router: Router,
        private readonly trainersService: TrainersService) {
    }

    async ngOnInit(): Promise<void> {
        if (this.pokemonsService.pokemons() === undefined) {
            console.log("hello")
            await this.pokemonsService.fetchPokemons().then( () => {
                    this.pokemonArray = this.initPokemons()
                    this.pageSlice = this.pokemonArray?.slice(0, 21)
                });
        }
        else{
            this.pokemonArray = this.initPokemons()
            this.pageSlice = this.pokemonArray?.slice(0, 21)
        }
    }


    initPokemons(): Pokemon[] | undefined {
        let pokemons: Pokemon[] = this.pokemonsService.pokemons()!;
        if (pokemons === undefined) return undefined
        for (let pokemon of pokemons!) {
            pokemon.name = pokemon.name[0].toUpperCase() + pokemon.name.substring(1)
            pokemon.id = parseInt(pokemon.url.split('/')[6]);
            pokemon.avatar = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + pokemon.id + '.png'
        }
        return pokemons!;
    }

    get pokemons(): Pokemon[] {
        return this.pokemonArray!;
    }

    catchPokemon(pokemon: Pokemon): void {
        alert("You catched " + pokemon.name)
        let trainer = this.sessionService.trainer;
        trainer?.pokemons.push(pokemon);
        this.sessionService.setTrainer(trainer!);
        this.trainersService.updateUser(trainer?.id!, trainer?.pokemons!)
    }

    onPageChange(event: PageEvent){
        const startIndex = event.pageIndex * event.pageSize;
        let endIndex = startIndex + event.pageSize;
        if (endIndex > this.pokemons?.length!) {
            endIndex = this.pokemons?.length!
        }
        this.pageSlice = this.pokemons?.slice(startIndex, endIndex);
    }
}
