import {Component, OnDestroy, OnInit} from '@angular/core';
import {TrainersService} from "../services/trainers.service";
import {Router} from "@angular/router";
import {SessionService} from "../services/session.service";
import {AuthGuardService} from "../services/auth-guard.service";
import {AuthService} from "../services/auth.service";

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {

    constructor(
        private readonly router: Router,
        private readonly trainersService: TrainersService,
        private readonly authService: AuthService
    ) {
    }

    ngOnInit(): void {
        if (this.authService.isAuthenticated())
            this.router.navigate(['catalogue']);
    }

    ngOnDestroy(): void {
    }

    onClickSubmit(data: any): void {
        this.trainersService.authenticate(data.username, async () => {
            await this.router.navigate(['catalogue']);
        })
    }


}
