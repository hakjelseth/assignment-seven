import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {TrainerPageComponent} from './trainer-page/trainer-page.component';
import {PokemonCatalogueComponent} from './pokemon-catalogue/pokemon-catalogue.component';
import {HttpClientModule} from "@angular/common/http";
import {HeaderComponent} from './header/header.component';
import {AppRoutingModule} from "./app-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatPaginatorModule} from "@angular/material/paginator";
import { FooterComponent } from './footer/footer.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginPageComponent,
        TrainerPageComponent,
        PokemonCatalogueComponent,
        HeaderComponent,
        FooterComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatPaginatorModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
