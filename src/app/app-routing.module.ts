import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoginPageComponent} from "./login-page/login-page.component";
import {PokemonCatalogueComponent} from "./pokemon-catalogue/pokemon-catalogue.component";
import {TrainerPageComponent} from "./trainer-page/trainer-page.component";
import { AuthGuardService as AuthGuard } from "./services/auth-guard.service";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LoginPageComponent
    },
    {
        path: 'catalogue',
        component: PokemonCatalogueComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'profile',
        component: TrainerPageComponent,
        canActivate: [AuthGuard]
    }

]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
