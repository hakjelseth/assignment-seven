import {Component, OnInit} from '@angular/core';
import {TrainersService} from "../services/trainers.service";
import {SessionService} from "../services/session.service";
import {Trainer} from "../models/trainer.model";
import {Pokemon} from "../models/pokemon.model";

@Component({
    selector: 'app-trainer-page',
    templateUrl: './trainer-page.component.html',
    styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {
    trainer: Trainer | undefined;

    constructor(private readonly sessionService: SessionService,
                private readonly trainersService: TrainersService
    ) {
    }

    ngOnInit(): void {
        this.trainer = this.sessionService.trainer;
    }

    releasePokemon(pokemon: Pokemon){
        alert("You released " + pokemon.name)
        const index = this.trainer?.pokemons.indexOf(pokemon)
        this.trainer?.pokemons.splice(index!, 1);
        this.sessionService.setTrainer(this.trainer!);
        this.trainersService.updateUser(this.trainer?.id!, this.trainer?.pokemons!);
    }
}
