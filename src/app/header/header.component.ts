import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {SessionService} from "../services/session.service";
import {Trainer} from "../models/trainer.model";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    trainer: Trainer | undefined;
    constructor(private readonly router: Router,
                private readonly sessionService: SessionService) {
    }

    ngOnInit(): void {
    }

    get isLoggedIn(): boolean {
        return Boolean(this.sessionService.trainer);
    }

    goToProfile() {
        this.router.navigate(['profile']);
    }

    logOut() {
        this.sessionService.logout();
        this.router.navigate(['login']);
    }
}
