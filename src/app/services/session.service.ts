import { Injectable } from '@angular/core';
import {Trainer} from "../models/trainer.model";
import {Pokemon} from "../models/pokemon.model";

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    private _trainer: Trainer | undefined;
    private _pokemons: Pokemon[] | undefined;

    constructor() {
        const storedUser = localStorage.getItem('user')
        const storedPokemons = sessionStorage.getItem('pokemons')
        if (storedUser) {
            this._trainer = JSON.parse(storedUser) as Trainer;
        }
        if (storedPokemons){
            this._pokemons = JSON.parse(storedPokemons);
        }
    }

    get trainer(): Trainer | undefined {
        return this._trainer;
    }

    get pokemons(): Pokemon[] | undefined {
        return this._pokemons;
    }

    setTrainer(trainer: Trainer): void {
        this._trainer = trainer;
        localStorage.setItem('user', JSON.stringify(trainer));
    }

    setPokemons(pokemons: Pokemon[]): void {
        this._pokemons = pokemons;
        sessionStorage.setItem('pokemons', JSON.stringify(pokemons))
    }

    logout() {
        this._trainer = undefined;
        localStorage.removeItem('user');
    }
}
