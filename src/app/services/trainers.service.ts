import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Trainer} from "../models/trainer.model";
import {Observable, of} from "rxjs";
import {environment} from "../../environments/environment";
import {finalize, switchMap, tap} from "rxjs/operators";
import {SessionService} from "./session.service";
import {Pokemon} from "../models/pokemon.model";

const TRAINER_API = environment.TRAINER_API
const headers = {'X-API-Key': environment.TRAINER_API_KEY};

@Injectable({
    providedIn: 'root'
})
export class TrainersService {
    private _trainer: Trainer[] = [];
    private _error: string = '';
    constructor(private readonly http: HttpClient, private readonly sessionService: SessionService) {
    }

    private findByUsername(username: string): Observable<any> {
        return this.http.get(`${TRAINER_API}/?username=${username}`)
    }

    private createUser(username: string): Observable<any> {
        return this.http.post(`${TRAINER_API}`, { username, pokemons: [] }, { headers } )
    }

    public updateUser(Id: number, pokemons: Pokemon[]){
        return this.http.patch(`${TRAINER_API}/${Id}`, { pokemons }, { headers } )
            .subscribe((result: any) =>{
                console.log("Result" + result)
            })
    }

    public authenticate(username: string, onSuccess: () => void): void {
        this.findByUsername(username)
            .pipe(
                switchMap((trainers: Trainer[]) => {
                    if (trainers.length){
                        return of(trainers[0])
                    }

                    return this.createUser(username)
                }),
                tap((trainer: Trainer) => {

                })
            )
            .subscribe(
                (trainer: Trainer) => {
                    if (trainer.id) {
                        this.sessionService.setTrainer(trainer)
                        onSuccess()
                    }
                },
                (error: HttpErrorResponse) => {
                    this._error = error.message ;
                }
            )
    }
}
