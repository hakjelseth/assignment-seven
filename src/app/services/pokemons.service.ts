import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Pokemon} from "../models/pokemon.model";
import {SessionService} from "./session.service";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class PokemonsService {
    private _pokemons: Pokemon[] | undefined = [];
    private _error: string = '';

    constructor(
        private readonly http: HttpClient,
        private readonly sessionService: SessionService
    ) {
        this._pokemons = sessionService.pokemons
    }

    fetchPokemons(): Promise<Object> {
        const poke = this.http.get("https://pokeapi.co/api/v2/pokemon?limit=1118");

        poke
            .subscribe(pokemons => {
                this._pokemons = (pokemons as any).results;
                this.setPokemons()
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            });
        return poke.toPromise();
    }

    private setPokemons(): void {
        this.sessionService.setPokemons(this._pokemons!)
    }
    public pokemons(): Pokemon[] | undefined {
        return this._pokemons;
    }

    public error(): string {
        return this._error;
    }
}
